import java.util.ArrayList;

/*******************************
 * Autor:
 * Fecha:
 * Ciudad:
 * Descripción:
 *****************************/
public class Profesor{
    /*************
     * Atributos
     *************/
    private int id;
    private String nombre;
    private String materia;
    private int salario;

     /*************
     * Constructor
     *************/
    public Profesor(int id, String nombre, String materia){
        this.id = id;
        this.nombre = nombre;
        this.materia = materia;
    }
    
    public String toString(){
        String strProfesor = "Id: "+this.id+"\n";
        strProfesor += "Nombre: "+this.nombre+"\n";
        strProfesor += "Materia: "+this.materia+"\n";
        strProfesor += "Salario: "+this.salario+"\n";
        return strProfesor;
    }
    

     /*************
     * Consultores
     * (Getters)
     *************/
    public int getId(){
        return this.id;
    }

    public String getNombre(){
        return this.nombre;
    }

    public String getMateria(){
        return this.materia;
    }

    public int getSalario(){
        return this.salario;
    }
     /*************
     * Modificadores
     * (Setters)
     *************/
    public void setNombre(String nombre){
        this.nombre = nombre;
    }

    public void setMateria(String materia){
        this.materia = materia;
    }

    public void setSalario(int salario){
        this.salario = salario;
    }

    /*************
     * Acciones
     ***********/
    public int calcular_salario(int valor_hora, int horas_trabajadas){
        this.salario = valor_hora * horas_trabajadas;
        return this.salario;
    }

    public int salario_total(ArrayList<Integer> salarios){
        int total_salarios = 0;
        for(int i = 0; i < salarios.size(); i++){
            total_salarios += salarios.get(i);
        }
        return total_salarios;
    }

}