import java.util.ArrayList;

public class App {
    public static void main(String[] args) throws Exception {
        //arreglo_objetos();
        calcular_salarios();
    }

    public static void calcular_salarios(){
        ArrayList<Integer> salarios = new ArrayList<Integer>();
        for(int i = 0; i < 5; i++){
            salarios.add(1000 * i);
        }
        Profesor objProfesor = new Profesor(1, "Pedro", "Fisica");
        int total = objProfesor.salario_total(salarios);
        System.out.println("Total salarios: "+total);
    }

    public static void arreglo_objetos() {
        // Crear objeto de tipo Profesor
        Profesor objProfesor = new Profesor(1, "Andrés", "Matemáticas");
        objProfesor.calcular_salario(10, 120);
        // System.out.println(objProfesor);
        // Arreglo de objetos
        Profesor[] profesores = new Profesor[2];
        profesores[0] = objProfesor;
        profesores[1] = new Profesor(2, "Juan", "Fisica");
        // Acceder
        System.out.println(profesores[0].getMateria());

        System.out.println("----------------ArrayList----------");
        ArrayList<Profesor> arrayProfesores;
        arrayProfesores = new ArrayList<Profesor>();

        arrayProfesores.add(objProfesor);
        arrayProfesores.add(new Profesor(1, "Andrés", "Matemáticas"));
        // acceder
        System.out.println(arrayProfesores.get(0).getMateria());

        arrayProfesores.clear();
    }
}
